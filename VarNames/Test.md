Before you can use this patch script for init.h, you must at least once
execute the conversion [VarNames/Script](VarNames/Script) to produce the $PATCHFILE used in
this script. You have to enable it in the script before you call it
(\`filename != /dev/null\`). Make sure you change \`$PATCHFILE\` in
both scripts to point to the same file.

Then read the script '\#'-comments below for usage:

``` 
 save script to file (maybe call it "varspatcher")
 chmod a+x varspatcher
```

then go into a newly unpacked source dir of mutt:

``` 
 mv init.h org-init.h
 fullpathto.../varspatcher < org-init.h > init.h
```

and then build mutt as usual and try it with your converted config. If
all was fine, the patched mutt should work as usual with your config, if
the regular mutt worked fine, too.

If you encounter your converted config not to work smoothly, then report
feedback as explained in [VarNames/Script](VarNames/Script).

-----

    #!/usr/bin/perl -w
    ###
    # written 16 Aug 2006 by RadoQ
    #
    # This script is meant to use the $PATCHFILE from /Script to
    # replace the vars in init.h and so to produce a mutt binary that
    # will work _only_ with the new varnames _without_ any synonyms to
    # see whether your config was _correctly_ converted by /Script.
    #
    # Syntax: ./patchscript < old-init.h > new-init.h
    
    use strict;
    
    my $PATCHFILE='tr-list';        ### where the conversion list is saved from /Script.
    my $line='';
    my %trans=();
    
    open(TRANSLIST,"<$PATCHFILE") || die "Can't open file $PATCHFILE for init.h patch";
    while ($line = <TRANSLIST>) {
            if ($line =~ /:/) {
                    chomp($line);
                    $line =~ /^\s*(\S+)\s*:\s*(\S+)/o;
                    $trans{$1}=$2;
            }
    }
    close(TRANSLIST);
    
    ### skip regular stuff before the vars
    while (defined($line = <STDIN>) && ($line !~ m,\Q/*++*/\E,o)) {
            print $line;
    }
    print $line;
    
    ### now the gory stuff
    VARS:
    while (defined($line = <STDIN>) && ($line !~ m,\Q/*--*/\E,o)) {
    
    if ($line =~ /DT_SYN/) { next VARS; }
    if ($line =~ /^\s*\{/) {
            $line =~ m,"([^"]*)",o;
            if (exists($trans{$1})) { $line =~ s,"([^"]*)",'"'.$trans{$1}.'"',e ;}
    }
            print $line;
    }
    print $line;
    
    ### skip regular stuff after the vars
    while (($line = <STDIN>) ) {
            print $line;
    }
    
    ### EOF
