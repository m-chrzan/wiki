This is a flow of mail through the internet from a sender to a receiver;
as depicted is a fully-fleshed flow, though some steps are optional. The
simple picture is: sender - transfer - recipient: MUA - MTA - ... - MTA
- MUA, but there can be finer details.

| action | actor | pass to **next** actor by | step |
| ------ | ----- | ------------------------- | ---- |
| *Sender wants to send a message.*                 |
| write new email | **MUA** (mutt itself, and mutt-var **$editor**) | \\/ mutt-var `$sendmail` \\/ |
| queue & send | **local MTA** (optional) | \\/ SMTP \\/ |
| validate & accept | **ISP's MSA** | \\/ SMTP \\/ |
| *now email is stored at own ISP's MSA = ISP's MTA* |
| relay to next MTA | **internet MTA** | \\/ SMTP \\/ | this repeats for each MTA between source & destination |
| *now email arrives at remote ISP* |
| receive | **remote MTA** | mta-internal |
| deliver | **MDA** | \\/ rules, filters, forwards \\/ |
| store | ISP filesystem | delivery complete |
| *Recipient now wants to read the stored mail.* |
| action | actor | get from **previous** actor by |**Note:** The activity direction *changes*. So far the sender "*pushed*" the msg, now the receiver has to "*pull*" the msg. |
| access & store locally | **MRA** (optional) | /\\ IMAP, POP /\\ |
| read | **MUA** | /\\ local filesystem /\\ |
| ''Success: the message has gone from sender to recipient.\\ |

*Recipient may want to become the sender of a different message; start over.*

Several steps above are
optional:

* The local MTA is omitted on most desktop computers; instead, the MUA directly submits the message to the ISP's MSA.  
* The MSA/MTA distinction is relatively recent, and the ISP may not distinguish between user submissions and message transfers.  
* The MRA step is included as part of most desktop MUAs, though traditionally email was read directly from a local or shared (as by NFS) file,  
  and still is if one uses a shell account and mail spool on the server to read mail.  
  A separate MRA such as fetchmail is used to replicate the traditional "MUA doesn't handle delivery" design, and allows very light-weight mail checking;  
  in integrated MUAs it may be a separate thread or process.
