**Read this** before you write your questions to the public channels!  
It's in **your own interest** to respect those to get the best results for you.  
Ask **smart questions** to get fast and good answers.  
The easier you make it
for us, the readier we are to help you (and the more we will **want**
to).  

"**before**": help make docs more useful for you, your problem probably
has been already
solved!

* Check **manual.txt**, **[MuttGuide](MuttGuide)**, and **[MuttFaq](MuttFaq)** before you ask:
  + use search instructions of [MuttFaq](MuttFaq) if you don't know where to look.
* Search the **ARCHIVES** of all channels available: [MuttLists](MuttLists) and [MuttChannel](MuttChannel) list these.
* **Analyze yourself** what you can per **[DebugConfig](DebugConfig)** before you ask.
* If you've **already found** the answers on your own by now, then **tell
us** which docs helped you (not?)!

 Please **search** those **before** you repost what has been covered before!
 Save resources and time better spent dealing with new problems.

"**while**": Help us help you. We can help you only as much as you help
us understand you: "it doesn't work" or "what I see" is **not
enough**!!!

* Be **supportive**: tell us your **operating
environment** and what **made you think it
should work** as you expect?
  * **What**: is your **technical context and
setup**?
    + - is your **goal**, - did you expect (**why?**), - was the result instead?
    + - are your used **versions** of: application, **client,
server**, libs, external components, operating system, terminal?
  * **How** to reproduce: we can't read your mind nor see through your eyes: explain as if **to
a blind
person**.
    + Give exact **details** as you experience them (where you start, each single step: action + result), **not
your summaries**!
    + Be most **specific + clear + exact +
concrete +
complete** with your statements.
    + Don't guess, **verify**! Get facts, exclude ambiguity + misunderstanding.
  * **Why**: the more you provide, the better we can help you, you decide how helpful you are to yourself.
    + For in depth reasons: "how to ask [smart
questions](http://www.catb.org/~esr/faqs/smart-questions.html)".
      - better read this! Complain about that text to [MuttChannel](MuttChannel), not the authors!.
* Show **commitment**: this is not a (paid) service hot-line, this is a do-it-yourself environment.
  * You are supposed to do the actual work, you get advice and pointers.
  * Look up references yourself, use existing docs, learn to help yourself.
* Be **respectful +
merciful** (with yourself + fellows): answers are favours, not your right, and not all of us know everything, so beware of sub-optimal answers.
* Be **patient**: just wait, eventually someone will answer, don't count on instant reaction.
  * This is a global channel: people live in different timezones, more or better response might happen at other times.
  * See [MuttMaps](MuttMaps) and
   [WorldTimeZone](http://www.worldtimezone.com/index24.php) or
   [TimeAndDate](http://www.timeanddate.com/worldclock/sunearth.html) or
   [WorldClock](http://www.worldclock.net/zeitzonenkarte.php)

"**after**": be prepared to give back to others.

* When you've been helped, please stay longer to **help
others** in the [MuttChannel](MuttChannel), [MuttLists](MuttLists)!

"**technical**": don't annoy bystanders!

* Every channel has its own **additional specific technical
clues** to make all sides happier: the ones asking for help and those granting it.
* Please return to the referring page and read those, too, before you ask.
