Hooks are a feature of mutt that allow for certain dynamic / automatic
behavior.  
**NOTE:** commands / settings are **NOT**
restored after execution / processing of a hook. You have to provide a
"default" match, that will catch everything that doesn't match your
specific case. However, do *not* provide a resetting "default" match for
hooks which do not run a command (and change a setting).

### mbox-hook foldername-regexp foldername

You can specify more than one "move-target" for read messages when you
have more than one different "inbox" folders defined by **mailbox**-cmd.
This is useful when you use "procmail" to sort incoming email
automatically into separate folders and need for each their own "read"
folder.

### folder-hook foldername-regexp command

  - Every folder-hook that matches is executed.
  - The order matters: the latter overwrites the former.
  - You need "." as the default to initialize for all cases that are not
    explicitly set in the following hooks.
  - Applies only when you actually **open** the folder, just accessing/
    listing it in the folder-browser is not enough.

