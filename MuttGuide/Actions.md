Once you've gotten mutt up and running, you need to configure your mutt
application for interactive use.

## Modes

Normally, mutt starts in "index" mode, displaying the content of your
"inbox" (see /Folders). [br](br "wikilink") Depending on your course of
action, you can enter some other modes, here the full list:

### primary modes

  - "index": list the emails of an opened folder, it's the main mode.  
    "pager": display a selected e-mail.  
    "attach": list the parts of MIME attachments.  
    "compose": set up a new email to be sent out.  
    "browser": directory listing when searching for folder to change to.

### sub-modes

  - "alias": list aliases (in mutt, your collection of aliases is your
    address book) to choose from.  
    "query": often used to reference a database, a common protocol being
    LDAP.  
    "postpone": list of postponed e-mails to selecting for editing.  
    "pgp": list of PGP or GnuPG keys, depending on your configuration.  
    "editor": the input line when you are supposed to enter some text.

Each mode has its own set of actions, which can be bound to keys of your
choice using the 'bind' command (see /Syntax). **To see a list of
current key bindings for a given mode, hit '?' (a.k.a. 'the menu').** It
will show you all available functions, even those unbound. There is a
set of generic bindings that will work by default in every mode unless a
specific context binding overrides the keys. You might want to have some
[CheatSheets](CheatSheets) for some often used default bindings.

Some actions are available in more than one mode, and most of those
commands are shared between "index" and "pager". Yet, to control the
same action in both modes with the same key requires that you configure
the key for both separately.

`* bind index K mail`  
`* bind pager K mail`

## Tagging: applying a <function> to multiple messages

There are two special commands available in several modes: **tag,
tag-prefix**. With "tag" you select a list of messages to which a
subsequent **built-in** `<function>` should be applied. As long as you
don't change the mode (or folder in index mode), you may work with
single messages on and off this tagged list before you apply a
<function> to the list. Invoking a `<function>` as usual applies only to
the currently selected item. If you apply `<tag-prefix>`, then the next
<function> applies to the whole list! There is also `<tag-prefix-cond>`,
which applies a following `<function>` only of there is a list! Sometimes
you might want it to work anyway, sometimes you don't: choose carefully
when to use what.

Also RTFM about "auto\_tag, delete\_untag, print\_split, print\_decode,
pipe\_split, pipe\_decode" for your options how to adjust `<tag-prefix>`
behavior.

## Macros

You can combine a sequence of actions with a single keystroke by using
macros. They are defined like regular key bindings, except that they
take not only a single function as argument, but a series of keys or
"`<functions>`".

You can specify literal keys, which is extremely useful to fill in
dialog input that you normaly enter manually. However, you should limit
literal keys exactly to this purpose of editor input. For the
portability of a macro into the environment of another user's
keybindings it is recommended that you use the "`<function>`" syntax to
call actions rather than your currently bound key for that function,even
though it looks more complicated.

* bad:  `macro pager K 'c+bla\nm'`  
* good: `macro pager K '<change-folder>+bla<enter><mail>'`

See [/Macros](MuttGuide/Macros) for a longer introduction.

## TAB completion in "editor" mode

In every prompt where you have to enter a folder or alias, mutt will
autocomplete a partially given path (item for alias) by pressing the
**TAB** key. This works even with the shortcuts & remote folders (see
[/Folders](MuttGuide/Folders)).

When mutt has reached a point where it can't auto-complete on its own
(no more or too many matches), you can hit **TAB** another time
("TAB-TAB") to make mutt enter "**folder browser**" mode: mutt shows all
entries matching the given (partial) path. That's the same as "c?" ==
start with empty partial path. Have a look for "**$mask**" to set
personal filters for files displayed.

Note: if the browsing after "c?" or "c\<TAB\>\<TAB\>" ends in a
**different** directory than you started, then the internal "browsing
directory" for the **next directory scan** will be set to that **last
browsed directory**. This directory is also used as **current relative**
for any file operation **without full path**. The initial "browsing
directory" is set to the value of **$folder**. To make sure that you
start out your browsing with the "$folder" base even after changing
dirs, always use "+" or "=" as initial path to be completed! (see
[/Folders](MuttGuide/Folders))

`c+<TAB><TAB>`

When in "folder browser", you can hit '?' to see its specific key
bindings for extra functions. Some of them are only useful and therefore
available with IMAP enabled. Others are generally useful, like going to
"mailboxes" list, which is bound to <TAB> by default: to get there from
the start you have to hit

`c?<TAB>` or c`<TAB><TAB><TAB>`

## generic Movement

There is a myriad of ways to move around your screen. When you hit '?'
for the list of functions, you'll notice a lot of functions with "next-"
and "previous-" in
them:

`page, line, thread, subthread, new, unread, new-then-unread, undeleted, entry`

Most important to remember is the distinction between "undeleted" and
"entry", so that you can move to marked as deleted messages, which
normally are jumped over.  
The difference between "unread" and "new" is that if instructed via
**$mark\_old**, then mutt will mark new but yet unread messages as "old"
when you leave the folder.

At last, of course you have absolute movement,
too:

* jump to specific number simply by entering it.  
* jump to top, bottom, middle of page, first/ last entry of list.  
* scroll current message to top, bottom, middle of screen.

### special system commands

* ":" let's you enter a muttrc cmd to be executed once (useful "**`:set
?var`**" to see current value)  
* "!" invokes a command-shell, returns to mutt when you exit the shell  
* quit: end mutt after saving changes to current folder  
* exit: end mutt **without** saving changes to current folder  
* create-alias, edit current message, 

## Index + Pager mode

### folder management

See also [/Folders](MuttGuide/Folders).

* un-/delete $save_empty  
* flags  
* auto_tag, tagging, prefix,  
* simple_search, `<limit>` cmd, patterns list (+ [PatternQuoting](PatternQuoting))  
* save: confirmappend, confirmcreate  
* save /copy /move (see [MuttFaq/Action](MuttFaq/Action))  
* change: delete, move

### msg management

* view (src or MIME) -> /Attachments "pager" + "attach" mode  
* auto_view, $forward_decode  
* pipe_sep, pipe_split, print_*  
* toggle weeding, toggle quoted, skip quoted: quote-regexp

### get/ check new mail

* [MuttFaq/Folder](MuttFaq/Folder) -> mailcheck, timeout  
* pop internal, URL ...

## Compose msg

* mail, reply (group, list) -> /Compose "alias", "compose", "query", "postpone", "pgp" + "attach" mode  
* bounce, resend,  
* forward: various mime_* + forward_* vars

As long as $mime\_forward is unset (the default), attachments will be
automatically inserted into the body of the message. On the other hand,
you may override the default, and either be prompted to forward the
message ('ask-'yes/no) or do it automatically ('yes').

Forwarding messages as attachments has its advantages and disadvantages.
The advantage is that you are sending a complete copy of the message to
the recipient. All headers are kept intact, which can be helpful for
reporting spammers. On the other hand, if you want to be able to see and
work with the original message (ideal for mailing lists and casual
e-mails), then it is safe to leave $mime\_forward alone.

Keep in mind that $mime\_forward is a 'quadoption' variable, and by
setting it to 'ask-yes' or 'ask-no', you will be prompted each time you
forward.

### edit input fields

### browser

IMAP/maildir dir vs. folder = space vs. enter = view vs. select
