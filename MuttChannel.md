The official Mutt channel is **\#mutt** on
**[irc.freenode.net](http://www.freenode.net/)** (what is
[IRC](http://www.irchelp.org/)?). If you have questions that cannot be
answered by RTFM, join **\#mutt** and ask there. If you like, stay some
time to help others by answering some questions yourself. Or even stay
permanently :-)

Use the above shown connection data
to join with any IRC-client like [irssi](http://www.irssi.org/),
[XChat](http://www.xchat.org/), [Gaim](http://gaim.sourceforge.net/) or
[ChatZilla](http://www.hacksrus.com/~ginda/chatzilla/). If you're too
lazy or limited in admin-power, try
[webchat](http://webchat.freenode.net/) instead. We collect some
fortunes of [MuttQuotes](MuttQuotes).

Due to excessive spam, the \#mutt channel is restricted to registered nicks.
Please see the instructions at https://freenode.net/kb/answer/registration
for how to register your nick on freenode.

-----

## Channel Guidelines

**REQUIRED: read [BeforeYouAsk](BeforeYouAsk)!!!**

If you don't, the help you get will not be as good (speed + quality) as
it could be!

* Just **ask your stuff**, don't ask if we're "alive" nor "anyone
  using XYZ"! Stay mutt-related on topic and all will be fine.
* **Don't flood** the channel by pasting lots of lines (up to 4 lines is ok).
  If you have to paste a lot of lines, use a paste service such as
  https://ptpb.pw/ or http://termbin.com/
* Disable **auto-away** messages & **nick-changes**! Use the **silent**
  /away feature of your client manually, nothing else!
* Be **patient**: don't re-post too often, once every 12h is ok.

**REQUIRED: read [BeforeYouAsk](BeforeYouAsk)!!!**

Before you ask for help, please introduce yourself with a \#mutt
service ticket number (\#mst), which you choose in the range 0-999, to
show that you've understood all of the above. ;-)

-----

If you like, join [MuttMaps](MuttMaps).

-----

## \#mutt Statistics

Myon has put up a **[\#mutt page](http://www.df7cb.de/irc/mutt/)** where
you can find fancy statistics on activity of **\#mutt** and
**\#procmail**:

|| [yesterday\&today](http://www.df7cb.de/irc/mutt/mutt-1.html)|| [last
week](http://www.df7cb.de/irc/mutt/mutt-week.html) || [last
month](http://www.df7cb.de/irc/mutt/mutt-month.html) || [full
log](http://www.df7cb.de/irc/mutt/mutt-full.html) ||
