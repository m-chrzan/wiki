### My settings are messed up after hooks are executed, they're not restored?

Right, hooks operate "one-way": if the condition is triggered, the given
cmd(s) is/are executed once, that's it! If you want to "undo" effects
for "normal" operation, you have to define what "normal" operation is by
giving a "default" hook: the pattern given to the hook must match
*always*, i.e. "." (dot).

Note, the **order** is important: defaults come first for hooks that
execute all matching patterns, but last for those which execute just the
first match! [RTFM]([MuttWiki](home) "wikilink")
carefully!

### Why get the commands given in hooks crippled, some are skipped?

### Why gives muttrc errors for commands specified in hooks at startup?

### How do I make a complex regular expression containing **"(|)" and "\\"** in a \*-hook/search work?

### I already doublequote stuff for patterns, but my macro still sucks ass!

Keep in mind that when an argument for a hook contains special
characters (in particular **spaces** and ";"), in most cases that
argument must be quoted. You probably want to read [PatternQuoting](PatternQuoting) for
help on several levels of quoting that hooks require.
