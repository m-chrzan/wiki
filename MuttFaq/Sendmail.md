### "Error sending message, child exited", what's the deal?

Mutt passes the mail to **$sendmail** for delivery (see [MailConcept](MailConcept)),
but this time the MTA refused to accept it from the MUA, or while
processing it some error occurred.

Make sure you have **/usr/lib/sendmail or /usr/sbin/sendmail** installed
and your MTA properly configured (read the MTAs docs carefully). Make
sure you get sendmail debug-information stored somewhere, for example
via **/etc/syslog**. Try to call **sendmail** directly from the
command-line for more clues.

And specify the **correct** path in muttrc.

### Mail doesn't arrive / bounces, how can I figure out why?

First have a look at the logfiles of your
[sendmail-MTA]([MailConcept](MailConcept) "wikilink"). Normally, status msgs are logged
via **syslog**, RTFM of "syslog" and your MTA to find the location and
analyze potential error messages.

Another thing you can do is turn on DSN (delivery system notification)
via the **$dsn\_\*** vars, and then check the returned headers. When
inspecting headers, always use the full headers, not the weeded (cleared
by "ignore"s).

### Why is it so slow when I send an email?

The default behavior of mutt is to wait until the mail is effectively
sent. You can disable this by having:

`set sendmail_wait=-1`

Put the line above to your .muttrc and mutt will return the hand without
waiting for the email to be sent. However, delivery feedback is then
delayed or has to be acquired by other means, see above.

### How do I configure Mutt to use a remote SMTP server to send mail?

Mutt can't do so directly (read the [MailConcept](MailConcept) to understand why this
won't change). You must specify a local MTA to do it. The MTA must have
a sendmail compatible interface to be used with **$sendmail**. See
[SendmailAgents](SendmailAgents) for which to use, configure, install.

Update: Recent versions (1.5.x) of Mutt has built-in SMTP support.

If the smtp option is enabled during the compilation, you just need to
add the following lines in you ~/.muttrc or ~/.mutt/muttrc

``` 
    set smtp_url="smtp://yourusername@smtp.example.com:587/"
    set smtp_pass="Your1!Really2@AweSome3#Password"
```

If you omit the smtp\_pass, Mutt will ask you to enter the password when
sending mails.

For older versions of mutt without SMTP capability, one simple
alternative is the sendmail.py
script: http://www.ynform.org/w/Pub/SendmailPy

### I need to use different SMTP servers for several accounts, how to manage that?

As you've learned above, you must tell your local MTA which remote MTA/
SMTP server to use for relay. You can change the invocation of
**$sendmail** "on the fly" by using "macros" and/ or the various hooks
to specify a separate config-file per different SMTP
server.

`... set sendmail="/usr/sbin/sendmail -oem -oi -C /etc/other/sendmail-config"`

Standard sendmail apparently doesn't allow to override only the
SMART\_HOST / mailhub of a config on the command-line, so you have to
use alternative config file with adjusted entries. More user-friendly
solutions can be found on
LightSMTPagents.

### SMTP authentication (APOP, ...) times out on me, how to save retyping password?

There isn't much mutt can do about it when you use the preferred way of
external MTA (SMTP agent), because that's entirely an MTA problem, which
mutt isn't (see [MailConcept](MailConcept): hasn't been until recently). Maybe you're
more lucky with the new built-in SMTP service, otherwise check your
MTA's docs.

If you're confronted with frequent save-before-send operations to resend
stuff after re-authing, then consider using the "postpone" feature:
prepare and postpone them all, then when ready to send, simply recall
one-by-one and fire them off. This should work fast enough to stay below
even 1min timeouts.
