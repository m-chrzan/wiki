# 2017-12-21 NOTE: The wiki has been ported over from trac, and is in the process of being cleaned up.

This is the primary starting point for the *Mutt* Wiki.

Mutt is a **M**ail **U**ser **A**gent, see [MailConcept](MailConcept) for what that is
and is not. Feel free to add information to these pages, become an
efficient **[WikiWorker](WikiWorker)**!

| Stable | 2.0.3 2020-12-04 |
| ------ | ---------------- |

[![pipeline status](https://gitlab.com/muttmua/mutt/badges/master/pipeline.svg)](https://gitlab.com/muttmua/mutt/commits/master)

Documentation outside of this wiki can be found on the mutt home page:
<http://www.mutt.org/#doc>

* **Download** instructions: <http://www.mutt.org/download.html>  
* By default pressing **`F1`** loads the manual in mutt (unless your window manager eats `F1`).  
 * Otherwise find your **locally
installed** copy by executing this cmd: **`man mutt | grep manual.txt`**  
 * If you can't find them locally (often at `/usr/*/doc/mutt/manual.txt(.gz)`), then use the manuals at <http://www.mutt.org/#doc>

-----

## Starting points

These apply to the most recent mutt version (identified at the top of
this page) unless otherwise
noted.

* [MuttGuide](MuttGuide) for newbies, to learn *everything* from the basics to the advanced stuff. Or maybe a !UserStory for special cases instead.  
* [MuttFaq](MuttFaq) for *specific
issues* when you know the basics. Lists answers to frequently asked questions. Use [DebugConfig](DebugConfig) strategies for unknown problems.  
 * [MacroSamples](MacroSamples) and generic [ConfigTricks](ConfigTricks) of popular solutions to simplify recurring, mass, complex or tricky tasks.  
* [MuttTools](MuttTools) to expand MUA functionality.  
* [UpgradingTips](UpgradingTips) is a place to provide tips for upgrading to the latest version.  We encourage the community to contribute.  
* [Index of all wiki pages](pages)

## Mutt users

* [MuttMaps](MuttMaps) of mutters around the world.  
* [ConfigList](ConfigList) of people's configuration files for Mutt (muttrc).  
* [UserPages](UserPages) link you to more special, detailed, extra info, useful tools.  
* Visit official **IRC** [MuttChannel](MuttChannel) or join the [MuttLists](MuttLists). Use their "Paste" pages to work on examples.  
* [MuttAdvocacy](MuttAdvocacy) (why mutt sucks less). Enjoy [MuttQuotes](MuttQuotes) found in [MuttChannel](MuttChannel) and [MuttLists](MuttLists).

## Developing mutt and using the latest development code

* [MuttDev](MuttDev) provides a list of developer resources.
* To get a quick idea of the visible changes between releases, see [UPDATING](https://gitlab.com/muttmua/mutt/raw/master/UPDATING). (More detailed information may be found in the [Git
log](https://gitlab.com/muttmua/mutt/commits/master))  
* [SubmittingPatches](SubmittingPatches) tells you what to do when you want to submit your own patch, see [PatchList](PatchList) for existing third party patches for Mutt.  
* [RelevantStandards](RelevantStandards) used/ supported for inter-operability and independence.  
* [VarNames](VarNames) to track things to be done for a possible new naming scheme.  
* [MuttPeople](MuttPeople) who are busy with different aspects of making mutt useful.  
* [MuttPackages](MuttPackages) links to packaged version of Mutt in OS distributions
